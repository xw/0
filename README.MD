# 0

This project has created 1 MiB, 10 MiB and 100 MiB files for you for testing. You can also create your own large dummy files according to the manual below.

## Usage

Download 1 MiB file

```
https://gitlab.com/xw/0/raw/0/1
```

Download 10 MiB file

```
https://gitlab.com/xw/0/raw/0/10
```

Download 100 MiB file

```
https://gitlab.com/xw/0/raw/0/100
```

Download 1 GiB file

```
https://gitlab.com/xw/0/raw/0/1024
```

## Example

### Download

Download 10 MiB file on a Linux system:

```
wget -O "./10" "https://gitlab.com/xw/0/raw/0/10"
```

Use `curl` to download 10 MiB files on Windows 10 1803 Redstone 4 (April 2018 Update) and later:

```
curl -o ".\10" "https://gitlab.com/xw/0/raw/0/10"
```

### Create your own dummy zero-filled file locally

Create a 10MiB dummy file on a **Linux** or a **macOS** system:

```
fallocate -l 10M ./10
```

Another way:

```
dd if=/dev/zero of=./10 count=1024 bs=10240
```

Create a 10MiB dummy file on a **Windows** system:

```
fsutil file createnew .\10 10485760
```

## License

[CC0](https://gitlab.com/xw/0/-/blob/0/LICENSE)


![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)
